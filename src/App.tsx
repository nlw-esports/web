interface ButtonProps {
  title: string;
}

function Button(props: ButtonProps) {
  return <button>{props.title}</button>;
}

function App() {
  return (
    <div>
      <Button title="Click 1" />
      <Button title="Click 2" />
      <Button title="Click 3" />
      <Button title="Click 4" />
    </div>
  )
}

export default App
